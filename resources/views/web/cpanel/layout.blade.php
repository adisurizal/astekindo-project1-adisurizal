@php
$current_uri = Request::path();
$explode_uri = explode('/', $current_uri);
$full_url = request()->url();
$explode_uri2 = explode('/', $full_url);
function switch_page($locale) {
  $array = '';
  $full_url2 = request()->fullUrl();
  $explode_uri3 = explode('/', $full_url2);
  for ($i=1; $i < count($explode_uri3); $i++) {
    if ($i == 3) {
      $array .= '/'.$locale;
    } else {
      $array .= '/'.$explode_uri3[$i];
    }
  }
  return $array;
}
@endphp

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">

  <!-- OGP -->
  <meta property="og:title" content="@yield('title') | {{ $optional[1] }}">
  <meta property="og:url" content="{{ $optional[4] }}">
  <meta property="og:description" content="{{ $optional[2] }}">
  <meta property="og:image" content="{{ url($optional[0].'/vendor/astekindo') }}/img/p3sm_b.png">
  <meta property="og:type" content="website">

  <title>@yield('title') | {{ $optional[1] }}</title>

  <!-- favicon -->
  <link rel="shortcut icon" href="{{ url($optional[0].'/vendor/astekindo') }}/img/p3sm_b.png" />
  <link rel="apple-touch-icon" href="{{ url($optional[0].'/vendor/astekindo') }}/img/p3sm_b.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="{{ url($optional[0].'/vendor/astekindo') }}/img/p3sm_b.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="{{ url($optional[0].'/vendor/astekindo') }}/img/p3sm_b.png" />

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">

  <!-- CSS Libraries -->
  @if ($detect_mobile == true)
  <link rel="stylesheet" href="{{ url($optional[0].'/vendor/astekindo') }}/css/regular-preloader-mobile.css">
  @else
  <link rel="stylesheet" href="{{ url($optional[0].'/vendor/astekindo') }}/css/regular-preloader-desktop.css">
  @endif
  <link rel="stylesheet" href="{{ url($optional[0].'/vendor/astekindo') }}/css/new-tooltip.css">
  <link rel="stylesheet" href="{{ url($optional[0].'/vendor/stisla-master') }}/node_modules/chocolat/dist/css/chocolat.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ url($optional[0].'/vendor/stisla-master') }}/assets/css/style.css">
  <link rel="stylesheet" href="{{ url($optional[0].'/vendor/stisla-master') }}/assets/css/components.css">

</head>

<body class="sidebar-mini">
  <div class="preloader">
    <div class="loading">
      <img src="{{ url($optional[0].'/vendor/astekindo') }}/img/gif/loading2.gif"
      @if($detect_mobile == true) style="width:50%" @else style="width:20%" @endif>
    </div>
  </div>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">

        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <?php /* <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li> */ ?>
          </ul>
        </form>

        <ul class="navbar-nav navbar-right">
          <li class="dropdown dropdown-list-toggle">
            <a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg">
              <i class="fa fa-language" style="font-size:28px"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a href="{{ switch_page('id') }}" class="dropdown-item has-icon">
                Bahasa Indonesia
              </a>
              <a href="{{ switch_page('en') }}" class="dropdown-item has-icon">
                English
              </a>
            </div>
          </li>
          <li class="dropdown">
            <a href="" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
              <img alt="image" src="{{ url($optional[0].'/vendor/stisla-master') }}/assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
              <div class="d-sm-none d-lg-inline-block">{{ $user_online['username'] }}</div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item has-icon modal-modal" value="myprofile" style="cursor:pointer">
                <i class="far fa-user"></i> {{ ucwords(trans('cpanel.profile')) }}
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item has-icon text-danger modal-modal" value="signout" style="cursor:pointer">
                <i class="fas fa-sign-out-alt"></i> {{ ucwords(trans('cpanel.signout')) }}
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <?php /*
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a>P3SM</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a>P3SM</a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">cpanel</li>
          </ul>
        </aside>
      </div>
      */ ?>
      @yield('content')
      <footer class="main-footer">
        <div class="footer-left">
          <a href="{{ $optional[4] }}">{{ $optional[3] }}</a> &copy; {{ date('Y') }} <div class="bullet"></div> {{ $optional[7] }}
        </div>
      </footer>
    </div>
  </div>

  <div style="display: none;" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal fade" id="xcontainer"></div>
  <div style="display: none;" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal" id="modalloader">
    <div class="preloader2">
      <div class="loading2">
        <img src="{{ url($optional[0].'/vendor/astekindo') }}/img/gif/loading2.gif"
        @if($detect_mobile == true) style="width:50%" @else style="width:20%" @endif>
      </div>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="{{ url($optional[0].'/vendor/stisla-master') }}/assets/js/stisla.js"></script>

  <!-- JS Libraries -->
  <?php // ini dimatikan karna menghalangi tooltip ?>
  <?php /* <script src="{{ url($optional[0].'/vendor/stisla-master') }}/node_modules/jquery-ui-dist/jquery-ui.min.js"></script> */ ?>
  <script src="{{ url($optional[0].'/vendor/astekindo') }}/js/preloader-stop.js" type="text/javascript"></script>
  <script src="{{ url($optional[0].'/vendor/astekindo') }}/js/modal-with-loader.js" type="text/javascript"></script>
  <script src="{{ url($optional[0].'/vendor/astekindo') }}/js/send-to-checkbox.js" type="text/javascript"></script>
  <script src="{{ url($optional[0].'/vendor/astekindo') }}/js/new-tooltip.js" type="text/javascript"></script>
  <script src="{{ url($optional[0].'/vendor/stisla-master') }}/node_modules/chocolat/dist/js/jquery.chocolat.min.js"></script>

  <!-- Template JS File -->
  <script src="{{ url($optional[0].'/vendor/stisla-master') }}/assets/js/scripts.js"></script>
  <script src="{{ url($optional[0].'/vendor/stisla-master') }}/assets/js/custom.js"></script>

  <!-- Page Specific JS File -->
  <?php // ini dimatikan karna terkait dengan jquery-ui.min.js ?>
  <?php /* <script src="{{ url($optional[0].'/vendor/stisla-master') }}/assets/js/page/components-table.js"></script> */ ?>

</body>
</html>
