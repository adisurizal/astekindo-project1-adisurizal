@extends('web.cpanel.layout')
@section('title', $title)
@section('content')

@php
$current_uri = Request::path();
$explode_uri = explode('/', $current_uri);
@endphp

<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
          <div class="card">
            <div class="card-body" style="text-align:center">
              @if ($detect_mobile == true)
              <img src="{{ $image_path }}" style="width:100%">
              <p style="font-size:16px; margin-top:20px">{{ $message_error }}</p>
              <button onclick="history.go(-1);" class="btn btn-info" style="margin-bottom:20px">{{ ucwords(trans('cpanel.return_to_previous_page')) }}</button>
              @else
              <img src="{{ $image_path }}" style="width:30%">
              <p style="font-size:20px; margin-top:20px">{{ $message_error }}</p>
              <button onclick="history.go(-1);" class="btn btn-info" style="margin-bottom:20px">{{ ucwords(trans('cpanel.return_to_previous_page')) }}</button>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection
