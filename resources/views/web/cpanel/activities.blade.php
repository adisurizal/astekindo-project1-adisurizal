@extends('web.cpanel.layout')
@section('title', ucwords(trans('cpanel.activities')))
@section('content')

@php
$current_uri = Request::path();
$explode_uri = explode('/', $current_uri);

$id_activities = $_GET['id'];
$uji_peserta_first = App\Models\Ujipeserta::where('id', $id_activities)->first();
$master_peserta = App\Models\Ujimspeserta::where('nik', $uji_peserta_first['nik'])->first();
$master_customer = App\Models\Ujimscustomer::where('id', $master_peserta['id_customer'])->first();

$date_now = date('Y-m-d');

$id_jadwal = $uji_peserta_first['id_jadwal'];
$uji_jadwal_rundown = App\Models\Ujijadwalrundown::where('id_jadwal', $id_jadwal)
                                                 ->where('tanggal', $date_now);
$uji_jadwal_rundown_count = $uji_jadwal_rundown->get()->count();

$uji_jadwal_first_x = App\Models\Ujijadwal::where('id', $id_jadwal)->first();
$instruktur = App\Models\Instruktur::where('id', $uji_jadwal_first_x['id_koordinator'])->first();
$personal = App\Models\Personal::where('id', $instruktur['id_personal'])->first();

if ($uji_jadwal_rundown_count == '0') {
  $hari_ke = '-';
  $tanggal = '-';
  $hari = '-';
  $modul_sesi1 = '-';
  $modul_sesi2 = '-';
  $modul_sesi3 = '-';
  $modul_sesi4 = '-';
  $modul_sesi5 = '-';
  $narasumber1 = '-';
  $narasumber2 = '-';
  $narasumber3 = '-';
  $narasumber4 = '-';
  $narasumber5 = '-';
} else {
  $uji_jadwal_rundown_first = $uji_jadwal_rundown->first();
  $hari_ke = $uji_jadwal_rundown_first['hari'];
  $tanggal = date('d-m-Y', strtotime($uji_jadwal_rundown_first['tanggal']));
  $tanggal_konversi_hari = date('D', strtotime($tanggal));
  $x = $tanggal_konversi_hari;
  if ($x == 'Sun') $hari = ucwords(trans('cpanel.sunday'));
  elseif ($x == 'Mon') $hari = ucwords(trans('cpanel.monday'));
  elseif ($x == 'Tue') $hari = ucwords(trans('cpanel.tuesday'));
  elseif ($x == 'Wed') $hari = ucwords(trans('cpanel.wednesday'));
  elseif ($x == 'Thu') $hari = ucwords(trans('cpanel.thursday'));
  elseif ($x == 'Fri') $hari = ucwords(trans('cpanel.friday'));
  elseif ($x == 'Sat') $hari = ucwords(trans('cpanel.saturday'));
  else $hari = '-';

  $modul_sesi1 = '-';
  $modul_sesi2 = '-';
  $modul_sesi3 = '-';
  $modul_sesi4 = '-';
  $modul_sesi5 = '-';
  $narasumber1 = '-';
  $narasumber2 = '-';
  $narasumber3 = '-';
  $narasumber4 = '-';
  $narasumber5 = '-';
}
@endphp

<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h style="font-size:20px">{{ ucwords(trans('cpanel.coaching_activities_for_participants')) }} <a style="font-weight:bold">{{ ucwords($uji_peserta_first['nama']) }}<a></h>
      @if ($detect_mobile != true)
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
          <a href="{{ url('/'.$locale.'/cpanel/schedule') }}">
            <h style="font-size:18px"><i class="fa fa-angle-left"></i> {{ ucwords(trans('cpanel.back')) }}</h>
          </a>
        </div>
      </div>
      @endif
    </div>
    <div class="section-body">
      @if(null !== (Session::get('session_message_cpanel_activities')) and Session::get('session_message_cpanel_activities') !== '')
      <div class="row">
        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
          <div class="alert {{ Session::get('session_class_cpanel_activities') }} alert-dismissible show fade">
            <div class="alert-body">
              <button class="close" data-dismiss="alert">
                <span>×</span>
              </button>
              {{ Session::get('session_message_cpanel_activities') }}
            </div>
          </div>
        </div>
      </div>
      @endif
      <div class="row">
        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-9 col-md-12 col-12 col-sm-12">
                  <div class="row">
                    <div class="col-lg-3 col-md-12 col-12 col-sm-12">
                      <div class="row" style="margin-top:10px">
                        <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                          <label class="col-form-label text-md-right col-12 col-md-12 col-lg-12">
                            {{ ucwords(trans('cpanel.customer')) }}
                          </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                          <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.customer')) }}" value="{{ ucwords($master_customer['nama_bu']) }}"
                          data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                          {{ ucwords(trans('cpanel.customer')) }} :<br>
                          {{ ucwords($master_customer['nama_bu']) }}
                          ">
                        </div>
                      </div>
                      <div class="row" style="margin-top:10px">
                        <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                          <label class="col-form-label text-md-right col-12 col-md-12 col-lg-12">
                            {{ ucwords(trans('cpanel.days_to')) }}
                          </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                          <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.days_to')) }}" value="{{ $hari_ke }}"
                          data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                          {{ ucwords(trans('cpanel.days_to')) }} :<br>
                          {{ $hari_ke }}
                          ">
                        </div>
                      </div>
                      <div class="row" style="margin-top:10px">
                        <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                          <label class="col-form-label text-md-right col-12 col-md-12 col-lg-12">
                            {{ ucwords(trans('cpanel.date')) }}
                          </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                          <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.date')) }}" value="{{ $tanggal }}"
                          data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                          {{ ucwords(trans('cpanel.date')) }} :<br>
                          {{ $tanggal }}
                          ">
                        </div>
                      </div>
                      <div class="row" style="margin-top:10px">
                        <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                          <label class="col-form-label text-md-right col-12 col-md-12 col-lg-12">
                            {{ ucwords(trans('cpanel.day')) }}
                          </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                          <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.day')) }}" value="{{ $hari }}"
                          data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                          {{ ucwords(trans('cpanel.day')) }} :<br>
                          {{ $hari }}
                          ">
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-12 col-sm-12">
                      @php
                      $number=1;
                      $uji_jadwal_rundown_xx1 = App\Models\Ujijadwalrundown::where('id_jadwal', $id_jadwal)->where('tanggal', $date_now);
                      foreach ($uji_jadwal_rundown_xx1->get() as $data_xx1):
                      $id_modul_xx1 = $data_xx1->id_modul;
                      $ms_modul_xx1 = App\Models\Msmodul::where('id', $id_modul_xx1)->first();
                      @endphp
                      <div class="row" style="margin-top:10px">
                        <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                          <label class="col-form-label text-md-right col-12 col-md-12 col-lg-12">
                            {{ ucwords(trans('cpanel.session_module')) .' '. $number }}
                          </label>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                          <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.session_module')) .' '. $number }}" value="{{ $ms_modul_xx1['modul'] }}"
                          data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                          {{ ucwords(trans('cpanel.session_module')) .' '. $number }} :<br>
                          {{ $ms_modul_xx1['modul'] }}
                          ">
                        </div>
                      </div>
                      @php
                      $number++;
                      endforeach;
                      @endphp
                    </div>
                    <div class="col-lg-3 col-md-12 col-12 col-sm-12">
                      @php
                      $numberxx2=1;
                      $uji_jadwal_rundown_xx2 = App\Models\Ujijadwalrundown::where('id_jadwal', $id_jadwal)->where('tanggal', $date_now);
                      foreach ($uji_jadwal_rundown_xx2->get() as $data_xx2):
                      @endphp
                      <div class="row" style="margin-top:10px">
                        <div class="col-lg-7 col-md-12 col-12 col-sm-12">
                          <label class="col-form-label text-md-right col-12 col-md-12 col-lg-12">
                            {{ ucwords(trans('cpanel.interviewees')) .' '. $numberxx2 }}
                          </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-12 col-sm-12">
                          <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.interviewees')) .' '. $numberxx2 }}" value="{{ ucwords($data_xx2['nama_narsum']) }}"
                          data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                          {{ ucwords(trans('cpanel.interviewees')) .' '. $numberxx2 }} :<br>
                          {{ ucwords($data_xx2['nama_narsum']) }}
                          ">
                        </div>
                      </div>
                      @php
                      $numberxx2++;
                      endforeach;
                      @endphp
                    </div>
                    <div class="col-lg-3 col-md-12 col-12 col-sm-12">
                      <div class="row" style="margin-top:10px">
                        <div class="col-lg-7 col-md-12 col-12 col-sm-12">
                          <label class="col-form-label text-md-right col-12 col-md-12 col-lg-12">
                            {{ ucwords(trans('cpanel.participants_name')) }}
                          </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-12 col-sm-12">
                          <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.participants_name')) }}" value="{{ ucwords($master_peserta['nama']) }}"
                          data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                          {{ ucwords(trans('cpanel.participants_name')) }} :<br>
                          {{ ucwords($master_peserta['nama']) }}
                          ">
                        </div>
                      </div>
                      <div class="row" style="margin-top:10px">
                        <div class="col-lg-7 col-md-12 col-12 col-sm-12">
                          <label class="col-form-label text-md-right col-12 col-md-12 col-lg-12">
                            {{ ucwords(trans('cpanel.participants_cellphone')) }}
                          </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-12 col-sm-12">
                          <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.participants_cellphone')) }}" value="{{ $master_peserta['no_hp'] }}"
                          data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                          {{ ucwords(trans('cpanel.participants_cellphone')) }} :<br>
                          {{ $master_peserta['no_hp'] }}
                          ">
                        </div>
                      </div>
                      <div class="row" style="margin-top:10px">
                        <div class="col-lg-7 col-md-12 col-12 col-sm-12">
                          <label class="col-form-label text-md-right col-12 col-md-12 col-lg-12">
                            {{ ucwords(trans('cpanel.coordinators_name')) }}
                          </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-12 col-sm-12">
                          <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.coordinators_name')) }}" value="{{ ucwords($personal['nama']) }}"
                          data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                          {{ ucwords(trans('cpanel.coordinators_name')) }} :<br>
                          {{ ucwords($personal['nama']) }}
                          ">
                        </div>
                      </div>
                      <div class="row" style="margin-top:10px">
                        <div class="col-lg-7 col-md-12 col-12 col-sm-12">
                          <label class="col-form-label text-md-right col-12 col-md-12 col-lg-12">
                            {{ ucwords(trans('cpanel.coordinators_cellphone')) }}
                          </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-12 col-sm-12">
                          <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.coordinators_cellphone')) }}" value="{{ ucwords($personal['hp_wa']) }}"
                          data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                          {{ ucwords(trans('cpanel.coordinators_cellphone')) }} :<br>
                          {{ ucwords($personal['hp_wa']) }}
                          ">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-12 col-12 col-sm-12">
                  <div class="row">
                    <div class="col-lg-3 col-md-12 col-12 col-sm-12" style="margin-top:10px">
                      <div class="gallery gallery-md">
                        @if ($uji_peserta_first['f_ktp'] == null)
                        <div class="gallery-item" data-image="{{ url($optional[0].'/vendor/astekindo') }}/img/id-card.png"
                             href="{{ url($optional[0].'/vendor/astekindo') }}/img/id-card.png"
                             style="background-image: url(&quot;{{ url($optional[0].'/vendor/astekindo') }}/img/id-card.png&quot;);">

                        </div>
                        @else
                        <div class="gallery-item" data-image="{{ url($optional[0].'/vendor/astekindo') }}/img/example/{{ $uji_peserta_first['f_ktp'] }}"
                             href="{{ url($optional[0].'/vendor/astekindo') }}/img/example/{{ $uji_peserta_first['f_ktp'] }}"
                             style="background-image: url(&quot;{{ url($optional[0].'/vendor/astekindo') }}/img/example/{{ $uji_peserta_first['f_ktp'] }}&quot;);">

                        </div>
                        @endif
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-12 col-sm-12" style="margin-top:10px">
                      <div class="gallery gallery-md">
                        @if ($uji_peserta_first['foto'] == null)
                        <div class="gallery-item" data-image="{{ url($optional[0].'/vendor/astekindo') }}/img/user.ico"
                             href="{{ url($optional[0].'/vendor/astekindo') }}/img/user.ico"
                             style="background-image: url(&quot;{{ url($optional[0].'/vendor/astekindo') }}/img/user.ico&quot;);">

                        </div>
                        @else
                        <div class="gallery-item" data-image="{{ url($optional[0].'/vendor/astekindo') }}/img/example/{{ $uji_peserta_first['foto'] }}"
                             href="{{ url($optional[0].'/vendor/astekindo') }}/img/example/{{ $uji_peserta_first['foto'] }}"
                             style="background-image: url(&quot;{{ url($optional[0].'/vendor/astekindo') }}/img/example/{{ $uji_peserta_first['foto'] }}&quot;);">

                        </div>
                        @endif
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-12 col-sm-12" style="margin-top:10px">
                      <a href="" class="btn btn-icon icon-left btn-success" style="font-size:20px; font-weight:bold">
                        <i class="fa fa-play" style="padding-top:15px"></i><br>Start
                      </a>
                    </div>
                    <div class="col-lg-3 col-md-12 col-12 col-sm-12" style="margin-top:10px">
                      <a href="" class="btn btn-icon icon-left btn-danger" style="font-size:20px; font-weight:bold">
                        <i class="fa fa-stop" style="padding-top:15px"></i><br>Stop
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="table-responsive" style="margin-top:20px">
                <table class="table table-bordered">
                  <tr>
                    <th>{{ ucwords(trans('cpanel.number')) }}</th>
                    <th>{{ ucwords(trans('cpanel.days_to')) }}</th>
                    <th>{{ ucwords(trans('cpanel.day')) }}</th>
                    <th>{{ ucwords(trans('cpanel.date')) }}</th>
                    <th>{{ ucwords(trans('cpanel.session')) }}</th>
                    <th>{{ ucwords(trans('cpanel.module')) }}</th>
                    <th>{{ ucwords(trans('cpanel.name')) }}</th>
                    <th>{{ ucwords(trans('cpanel.teaching_materials')) }}</th>
                    <th>{{ ucwords(trans('cpanel.added_material')) }}</th>
                    <th>{{ ucwords(trans('cpanel.resource_person_duties')) }}</th>
                    <th>{{ ucwords(trans('cpanel.participant_assignment')) }}</th>
                    <th>{{ ucwords(trans('cpanel.pkl_videos')) }}</th>
                    <th>{{ ucwords(trans('cpanel.attendees_attend')) }}</th>
                    <th>{{ ucwords(trans('cpanel.quiz')) }}</th>
                    <th>{{ ucwords(trans('cpanel.test_results')) }}</th>
                    <th>{{ ucwords(trans('cpanel.remedial_results')) }}</th>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@php
session(['session_message_cpanel_activities' => '']);
@endphp

@endsection
