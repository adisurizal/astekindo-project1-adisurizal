@extends('web.cpanel.layout')
@section('title', ucwords(trans('cpanel.schedule_coaching_activities_pjk3_mandiri')))
@section('content')

@php
$current_uri = Request::path();
$explode_uri = explode('/', $current_uri);
@endphp

<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h style="font-size:20px">{{ ucwords(trans('cpanel.schedule_coaching_activities_pjk3_mandiri')) }}</h>
    </div>
    <div class="section-body">
      @if(null !== (Session::get('session_message_cpanel_schedule')) and Session::get('session_message_cpanel_schedule') !== '')
      <div class="row">
        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
          <div class="alert {{ Session::get('session_class_cpanel_schedule') }} alert-dismissible show fade">
            <div class="alert-body">
              <button class="close" data-dismiss="alert">
                <span>×</span>
              </button>
              {{ Session::get('session_message_cpanel_schedule') }}
            </div>
          </div>
        </div>
      </div>
      @endif
      <div class="row">
        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
          <div class="card">
            <div class="card-body">
              @php
              $uji_peserta_first = App\Models\Ujipeserta::where('user_id', $eloquent->first()['id'])->first();
              $master_peserta = App\Models\Ujimspeserta::where('nik', $uji_peserta_first['nik'])->first();
              $master_customer = App\Models\Ujimscustomer::where('id', $master_peserta['id_customer'])->first();
              @endphp
              <div class="form-row">
                <div class="form-group col-md-2">
                  <label for="customer">{{ ucwords(trans('cpanel.customer')) }}</label>
                  <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.customer')) }}" value="{{ $master_customer['nama_bu'] }}" readonly>
                </div>
                <div class="form-group col-md-2">
                  <label for="participant">{{ ucwords(trans('cpanel.participant')) }}</label>
                  <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.participant')) }}" value="{{ $master_peserta['nama'] }}" readonly>
                </div>
                <div class="form-group col-md-2">
                  <label for="participant">{{ ucwords(trans('cpanel.mobile_phone')) }}</label>
                  <input type="text" class="form-control" placeholder="{{ ucwords(trans('cpanel.mobile_phone')) }}" value="{{ $master_peserta['no_hp'] }}" readonly>
                </div>
              </div>

              @php
              $uji_peserta_get = App\Models\Ujipeserta::where('user_id', $eloquent->first()['id'])->get();
              @endphp
              <div class="table-responsive">
                @if ($uji_peserta_get->count() == 0)
                <div class="alert alert-light">
                  <div class="alert-title">Oops !!</div>
                  {{ ucfirst(trans('cpanel.no_schedule_is_available_yet')) }}
                </div>
                @else
                <table class="table table-bordered" style="border: 2px solid #dee2e6;">
                  <tr style="border: 2px solid #dee2e6;">
                    <th>{{ ucwords(trans('cpanel.number')) }}</th>
                    <th>PJK3</th>
                    <th>{{ ucwords(trans('cpanel.activity_number')) }}</th>
                    <th>{{ ucwords(trans('cpanel.field')) }}</th>
                    <th>{{ ucwords(trans('cpanel.coaching')) }}</th>
                    <th>{{ ucwords(trans('cpanel.schedule_of_activity')) }}</th>
                    <th>{{ ucwords(trans('cpanel.activity_personnel')) }}</th>
                    <th>{{ ucwords(trans('cpanel.province')) }}</th>
                    <th>TUK</th>
                    <th>{{ ucwords(trans('cpanel.number_of_participants')) }}</th>
                  </tr>
                  @php
                  $number = 1
                  @endphp
                  @foreach ($uji_peserta_get as $data)
                  <tr style="border: 2px solid #dee2e6;">
                    <td>
                      {{ $number }}
                    </td>
                    <td>
                      @php
                      $uji_jadwal = App\Models\Ujijadwal::where('id', $data->id_jadwal)->first();
                      $badan_usaha = App\Models\Badanusaha::where('id', $uji_jadwal['id_pjk3'])->first();
                      $ms_bidang = App\Models\Msbidang::where('id', $uji_jadwal['id_bidang'])->first();
                      $ms_bid_sertifikat_alat = App\Models\Msbidsertifikatalat::where('id', $uji_jadwal['id_sert_alat'])->first();
                      $ms_modul = App\Models\Msmodul::where('id_bid_srtf_alat', $ms_bid_sertifikat_alat['id'])->first();
                      $instruktur = App\Models\Instruktur::where('id', $uji_jadwal['id_koordinator'])->first();
                      $personal = App\Models\Personal::where('id', $instruktur['id_personal'])->first();
                      $msjenisnarasumber = App\Models\Msjenisnarasumber::where('id', $instruktur['id_jenis_narsum'])->first();
                      $msprovinsi = App\Models\Msprovinsi::where('id', $uji_jadwal['id_prov'])->first();
                      $mskota = App\Models\Mskota::where('id', $uji_jadwal['id_kota'])->first();
                      $mstuk_tl = App\Models\Mstuk::where('id', $uji_jadwal['id_tuk_tl'])->first();
                      $prov_tuk_tl = App\Models\Msprovinsi::where('id', $mstuk_tl['prov'])->first();
                      $city_tuk_tl = App\Models\Mskota::where('id', $mstuk_tl['kota'])->first();
                      $mstuk_o = App\Models\Mstuk::where('id', $uji_jadwal['id_tuk_o'])->first();
                      $prov_tuk_o = App\Models\Msprovinsi::where('id', $mstuk_o['prov'])->first();
                      $city_tuk_o = App\Models\Mskota::where('id', $mstuk_o['kota'])->first();
                      $uji_peserta_get2 = App\Models\Ujipeserta::where('id_jadwal', $data->id_jadwal)->get();
                      $ujijadwalpny = App\Models\Ujijadwalpny::where('id_jadwal', $uji_jadwal['id'])->get();
                      $susun = '-';
                      foreach ($ujijadwalpny as $data_x_susun1) {
                        $susun .= ', '.$data_x_susun1['nama_bu'];
                      }
                      $str_rep_susun1 = str_replace('-, ', '', $susun);
                      $susun2 = '-';
                      foreach ($uji_peserta_get2 as $data_x_susun2) {
                        $susun2 .= ', '.$data_x_susun2['nama'];
                      }
                      $str_rep_susun2 = str_replace('-, ', '', $susun2);
                      @endphp
                      <a data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="{{ ucwords(trans('cpanel.nickname')) }} :<br>{{ $badan_usaha->singkat_bu }} <br><br> {{ ucwords(trans('cpanel.fullname')) }} :<br>{{ $badan_usaha->nama_bu }} <br><br> {{ ucwords(trans('cpanel.organizer_name')) }} :<br>{{ $str_rep_susun1 }}">
                        {{ $badan_usaha['singkat_bu'] }}
                      </a>
                    </td>
                    <td>
                      <a href="{{ url('/'.$locale.'/cpanel/activities?id='.$data['id']) }}" data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="{{ ucwords(trans('cpanel.activity_number')) }} :<br>{{ $uji_jadwal['no_kgt'] }} <br><br> {{ ucwords(trans('cpanel.activity_date')) }} :<br>{{ date('d-m-Y', strtotime($uji_jadwal['tgl_awal'])) .' '. trans('cpanel.up_to') .' '. date('d-m-Y', strtotime($uji_jadwal['tgl_akhir'])) }} <br><br> {{ ucwords(trans('cpanel.type_of_marketing')) }} :<br> @if ($uji_jadwal['jns_mktg'] == '0') {{ ucwords(trans('cpanel.internal')) }} @elseif ($uji_jadwal['jns_mktg'] == '1') {{ ucwords(trans('cpanel.external')) }} @endif">
                        {{ $uji_jadwal['no_kgt'] }}
                      </a>
                    </td>
                    <td>
                      <a data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="{{ ucwords(trans('cpanel.nickname')) }} :<br>{{ $ms_bidang['singkat_bidang'] }} <br><br> {{ ucwords(trans('cpanel.fullname')) }} :<br>{{ $ms_bidang['nama_bidang'] }}">
                        {{ $ms_bidang['singkat_bidang'] }}
                      </a>
                    </td>
                    <td>
                      <a data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="{{ ucwords(trans('cpanel.certificate_code')) }} :<br>{{ $ms_bid_sertifikat_alat['kode_srtf_alat'] }} <br><br> {{ ucwords(trans('cpanel.certificate_name')) }} :<br>{{ $ms_bid_sertifikat_alat['nama_srtf_alat'] }} <br><br> {{ ucwords(trans('cpanel.number_of_days')) }} :<br>{{ $ms_modul['hari'] }} <br><br> {{ ucwords(trans('cpanel.number_of_jp')) }} :<br>{{ $ms_modul['jp'] }} <br><br> {{ ucwords(trans('cpanel.type_of_activity')) }} :<br> @if ($uji_jadwal['jns_kgt'] == '0') {{ ucwords(trans('cpanel.internal')) }} @elseif ($uji_jadwal['jns_ktg'] == '1') {{ ucwords(trans('cpanel.external')) }} @endif">
                        {{ $ms_bid_sertifikat_alat['kode_srtf_alat'] }}
                      </a>
                    </td>
                    <td>
                      {{ date('d-m-Y', strtotime($uji_jadwal['tgl_awal'])) .' '. trans('cpanel.up_to') .' '. date('d-m-Y', strtotime($uji_jadwal['tgl_akhir'])) }}
                    </td>
                    <td>
                      <a data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="{{ ucwords(trans('cpanel.coordinator_name')) }} :<br>{{ $personal['nama'] }} <br><br> {{ ucwords(trans('cpanel.coordinator_phonecell')) }} :<br>{{ $personal['hp_wa'] }} <br><br> {{ ucwords(trans('cpanel.status')) }} :<br>{{ $msjenisnarasumber['nama'] }}">
                        {{ $personal['nama'] }}
                      </a>
                    </td>
                    <td>
                      <a data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="{{ ucwords(trans('cpanel.short_name_province')) }} :<br>{{ $msprovinsi['nama_singkat'] }} <br><br> {{ ucwords(trans('cpanel.full_name_of_province')) }} :<br>{{ $msprovinsi['nama'] }} <br><br> {{ ucwords(trans('cpanel.abbreviation_of_city_name')) }} :<br>{{ $mskota['singkatan_kota'] }} <br><br> {{ ucwords(trans('cpanel.full_name_city')) }} :<br>{{ $mskota['nama'] }}">
                        {{ $msprovinsi['nama_singkat'] }}
                      </a>
                    </td>
                    <td>
                      <a data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                        {{ ucwords(trans('cpanel.written_tuk')) }} :<br>{{ $mstuk_tl['nama_tuk'] }}
                        <br>{{ ucwords(trans('cpanel.address')) }} :<br>{{ $mstuk_tl['alamat'] }}
                        <br>{{ ucwords(trans('cpanel.province')) }} :<br>{{ $prov_tuk_tl['nama_singkat'] }}
                        <br>{{ ucwords(trans('cpanel.city')) }} :<br>{{ $city_tuk_tl['nama'] }}
                        <br>{{ ucwords(trans('cpanel.contact_person')) }} :<br>{{ $mstuk_tl['kontak_p'] }}
                        <br>{{ ucwords(trans('cpanel.cellphone')) }} :<br>{{ $mstuk_tl['no_hp_kontak_p'] }}
                        <br>{{ ucwords(trans('cpanel.email')) }} :<br>{{ $mstuk_tl['email_kontak_p'] }}
                        <br>
                        ------------------------
                        <br>{{ ucwords(trans('cpanel.tuk_observation')) }} :<br>{{ $mstuk_o['nama_tuk'] }}
                        <br>{{ ucwords(trans('cpanel.address')) }} :<br>{{ $mstuk_o['alamat'] }}
                        <br>{{ ucwords(trans('cpanel.province')) }} :<br>{{ $prov_tuk_o['nama_singkat'] }}
                        <br>{{ ucwords(trans('cpanel.city')) }} :<br>{{ $city_tuk_o['nama'] }}
                        <br>{{ ucwords(trans('cpanel.contact_person')) }} :<br>{{ $mstuk_o['kontak_p'] }}
                        <br>{{ ucwords(trans('cpanel.cellphone')) }} :<br>{{ $mstuk_o['no_hp_kontak_p'] }}
                        <br>{{ ucwords(trans('cpanel.email')) }} :<br>{{ $mstuk_o['email_kontak_p'] }}
                        ">
                        {{ ucwords(trans('cpanel.written_tuk')) }} :
                        <br>
                        {{ $mstuk_tl['nama_tuk'] }}
                        <br><br>
                        {{ ucwords(trans('cpanel.tuk_observation')) }} :
                        <br>
                        {{ $mstuk_o['nama_tuk'] }}
                        <br><br>
                      </a>
                    </td>
                    <td>
                      <a data-html="true" data-toggle="tooltip" data-placement="right" data-original-title="
                        @if ($uji_peserta_get2->count() == '0')
                        {{ ucwords(trans('cpanel.number_of_participants')) }} :<br>{{ $uji_peserta_get2->count() }}
                        <br><br>{{ ucwords(trans('cpanel.participants_name')) }} :<br>{{ ucfirst(trans('cpanel.no_participants')) }}
                        @else
                        {{ ucwords(trans('cpanel.number_of_participants')) }} :<br>{{ $uji_peserta_get2->count() }}
                        <br><br>{{ ucwords(trans('cpanel.participants_name')) }} :<br>{{ ucwords($str_rep_susun2) }}
                        @endif">
                          {{ $uji_peserta_get2->count() }}
                        </a>
                    </td>
                  </tr>
                  @php
                  $number++
                  @endphp
                  @endforeach
                </table>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@php
session(['session_message_cpanel_schedule' => '']);
@endphp

@endsection
