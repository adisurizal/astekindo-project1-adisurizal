<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">

  <!-- OGP -->
  <meta property="og:title" content="{{ ucwords(trans('login.login')) }} | {{ $optional[1] }}">
  <meta property="og:url" content="{{ $optional[4] }}">
  <meta property="og:description" content="{{ $optional[2] }}">
  <meta property="og:image" content="{{ url($optional[0].'/vendor/astekindo') }}/img/p3sm_b.png">
  <meta property="og:type" content="website">

  <title>{{ ucwords(trans('login.login')) }} | {{ $optional[1] }}</title>

  <!-- favicon -->
  <link rel="shortcut icon" href="{{ url($optional[0].'/vendor/astekindo') }}/img/p3sm_b.png" />
  <link rel="apple-touch-icon" href="{{ url($optional[0].'/vendor/astekindo') }}/img/p3sm_b.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="{{ url($optional[0].'/vendor/astekindo') }}/img/p3sm_b.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="{{ url($optional[0].'/vendor/astekindo') }}/img/p3sm_b.png" />

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ url($optional[0].'/vendor/stisla-master') }}/node_modules/bootstrap-social/bootstrap-social.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ url($optional[0].'/vendor/stisla-master') }}/assets/css/style.css">
  <link rel="stylesheet" href="{{ url($optional[0].'/vendor/stisla-master') }}/assets/css/components.css">
</head>

<body>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="card card-primary">
              <div class="login-brand">
                <?php // you can use this class for code below : class="shadow-light rounded-circle" ?>
                <img src="{{ url($optional[0].'/vendor/astekindo') }}/img/p3sm_a.png" alt="logo" width="100" style="margin-bottom:-30px">
              </div>
              <?php /* <div class="card-header"><h4>{{ ucwords(trans('login.login')) }}</h4></div> */ ?>
              <div class="card-body">
                @yield('content')
                <div class="text-center mt-4 mb-3" style="padding-top:20px">
                  <div class="text-job text-muted">
                    <a class="modal-modal" value="language" style="cursor:pointer">
                      <i class="fa fa-globe-asia"></i> {{ ucfirst(trans('login.language')) }} <i class="fa fa-caret-down"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="mt-5 text-muted text-center">
              {{ ucfirst(trans('login.developed_by')) }} <a href="{{ $optional[4] }}">{{ $optional[3] }}</a> &copy; {{ date('Y') }}
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div style="display: none;" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal fade" id="xcontainer"></div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="{{ url($optional[0].'/vendor/stisla-master') }}/assets/js/stisla.js"></script>

  <!-- JS Libraies -->
  <script src="{{ url($optional[0].'/vendor/astekindo') }}/js/modal-no-loader.js" type="text/javascript"></script>

  <!-- Template JS File -->
  <script src="{{ url($optional[0].'/vendor/stisla-master') }}/assets/js/scripts.js"></script>
  <script src="{{ url($optional[0].'/vendor/stisla-master') }}/assets/js/custom.js"></script>

  <!-- Page Specific JS File -->
</body>
</html>
