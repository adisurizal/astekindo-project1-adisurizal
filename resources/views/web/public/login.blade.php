@extends('web.public.layout')
@section('content')

@if(null !== (Session::get('session_message_login')) and Session::get('session_message_login') !== '')
<div class="alert {{ Session::get('session_class_login') }}">
  <i class="fa {{ Session::get('session_icon_login') }}" style="padding-right:10px"></i>
  {{ Session::get('session_message_login') }}
</div>
@endif

<form method="POST" class="needs-validation" novalidate="">
  @csrf
  <div class="form-group">
    <?php /* <label for="username">{{ ucwords(trans('login.username')) }}</label> */ ?>
    <input type="text" class="form-control" name="username" tabindex="1" placeholder="{{ ucfirst(trans('login.username')) }}" required autofocus
    @if(null !== (Session::get('session_message_login')) and Session::get('session_message_login') !== '')
    value="{{ Session::get('session_data_login_username') }}"
    @endif
    >
    <div class="invalid-feedback">
      {{ ucfirst(trans('login.username_is_required')) }}
    </div>
  </div>

  <div class="form-group">
    <?php /* <label for="password">{{ ucwords(trans('login.password')) }}</label> */ ?>
    <input type="password" class="form-control" name="password" tabindex="1" placeholder="{{ ucfirst(trans('login.password')) }}" required
    @if(null !== (Session::get('session_message_login')) and Session::get('session_message_login') !== '')
    value="{{ Session::get('session_data_login_password') }}"
    @endif
    >
    <div class="invalid-feedback">
      {{ ucfirst(trans('login.password_is_required')) }}
    </div>
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
      {{ ucfirst(trans('login.login')) }}
    </button>
  </div>
</form>

@php
session(['session_message_login' => '']);
@endphp

@endsection
