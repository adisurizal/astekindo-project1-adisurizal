<div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">{{ ucfirst(trans('cpanel.confirm')) }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    <div class="modal-body" style="text-align:center">
      {{ ucfirst(trans('cpanel.notif_logout')) }}
    </div>
    <div class="modal-footer">
      <a href="{{ url('/'.$locale.'/secure/signout') }}" type="submit" class="btn btn-danger">{{ ucwords(trans('cpanel.yes_logout')) }}</a>
    </div>
  </div>
</div>
