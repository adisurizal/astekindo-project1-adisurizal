<div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">{{ ucfirst(trans('cpanel.page_not_yet_available')) }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    <div class="modal-body" style="text-align:center">
      {{ ucfirst(trans('cpanel.myprofile_notice1')) }}
    </div>
  </div>
</div>
