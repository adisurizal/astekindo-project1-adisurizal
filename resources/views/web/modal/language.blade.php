<div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">{{ ucfirst(trans('login.select_language')) }}</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="buttons">
        <a href="{{ url('/id/login') }}" class="btn btn-light btn-block">Bahasa Indonesia</a>
        <a href="{{ url('/en/login') }}" class="btn btn-light btn-block">English</a>
      </div>
    </div>
  </div>
</div>
