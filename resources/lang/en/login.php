<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

return [
  'login' => 'login',
  'username' => 'username',
  'insert_your_username' => 'insert your username',
  'password' => 'password',
  'insert_your_password' => 'insert your password',
  'developed_by' => 'developed by',
  'username_is_required' => 'username is required',
  'password_is_required' => 'password is required',
  'language' => 'english',
  'select_language' => 'select language',
  'incorrect_username_or_password' => 'incorrect username or password',
  'your_account_is_blocked' => 'your account is blocked. contact admin to unblock',
  'you_have_logged_in_on_another_device' => 'you have logged in on another device',
  'you_are_not_allowed1' => 'you are not allowed to enter, because you are not participant'
];
