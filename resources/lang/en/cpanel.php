<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

return [
  'cpanel' => 'cPanel',
  'version' => 'version',
  'profile' => 'profile',
  'signout' => 'sign out',
  'confirm' => 'confirm',
  'yes_logout' => 'yes, sign out',
  'notif_logout' => 'are you sure sign out now?',
  'coaching' => 'coaching',
  'cpanel_coaching' => 'cPanel coaching',
  'option' => 'option',
  'schedule_coaching_activities_pjk3_mandiri' => 'schedule & coaching activities PJK3 Mandiri',
  'customer' => 'customer',
  'participant' => 'participant',
  'mobile_phone' => 'mobile phone',
  'number' => 'number',
  'activity_number' => 'activity number',
  'field' => 'field',
  'coaching' => 'coaching',
  'schedule_of_activity' => 'schedule of activity',
  'activity_personnel' => 'activity personnel',
  'province' => 'province',
  'number_of_participants' => 'number of participants',
  'action' => 'action',
  'back' => 'back',
  'no_schedule_is_available_yet' => 'no schedule is available yet',
  'nickname' => 'nickname',
  'fullname' => 'fullname',
  'organizer_name' => 'organizer name',
  'activity_date' => 'activity date',
  'type_of_marketing' => 'type of marketing',
  'up_to' => 'up to',
  'internal' => 'internal',
  'external' => 'external',
  'certificate_code' => 'certificate code',
  'certificate_name' => 'certificate name',
  'number_of_days' => 'number of days',
  'number_of_jp' => 'number of JP',
  'type_of_activity' => 'type of activity',
  'short_name_province' => 'short name province',
  'full_name_of_province' => 'full name of province',
  'abbreviation_of_city_name' => 'abbreviation of city name',
  'full_name_city' => 'full name city',
  'coordinator_name' => 'coordinator name',
  'coordinator_phonecell' => 'coordinator phonecell',
  'status' => 'status',
  'activities' => 'activities',
  'written_tuk' => 'wriiten TUK',
  'tuk_observation' => 'TUK observation',
  'address' => 'address',
  'city' => 'city',
  'contact_person' => 'contact person',
  'cellphone' => 'cellphone',
  'email' => 'email',
  'page_not_yet_available' => 'page not yet available',
  'myprofile_notice1' => 'sorry, the profile page is not available at this time. but dont worry, our team will finish as soon as possible. thank you',
  'coaching_activities_for_participants' => 'coaching activities for participants',
  'access_denied' => 'access denied',
  'access_denied_notice1' => 'oops, you are not allowed to access this page. this could be because this page is already protected or you have missing a parameter.',
  'access_denied_notice1_part1' => 'oops, you are not allowed to access this page.',
  'access_denied_notice1_part2' => 'this could be because this page is already protected or you have missing a parameter.',
  'return_to_previous_page' => 'return to previous page',
  'days_to' => 'days to',
  'date' => 'date',
  'day' => 'day',
  'session_module' => 'session module',
  'interviewees' => 'interviewees',
  'participants_name' => 'participants name',
  'participants_cellphone' => 'participants cellphone',
  'coordinators_name' => 'coordinators name',
  'coordinators_cellphone' => 'coordinators cellphone',
  'start_date_and_time' => 'start date and time',
  'end_date_and_time' => 'end date and time',
  'session' => 'session',
  'module' => 'module',
  'name' => 'name',
  'teaching_materials' => 'teaching materials',
  'added_material' => 'added material',
  'resource_person_duties' => 'resource person duties',
  'participant_assignment' => 'participant assignment',
  'pkl_videos' => 'PKL videos',
  'attendees_attend' => 'attendees attend',
  'quiz' => 'quiz',
  'test_results' => 'test results',
  'remedial_results' => 'remedial results',
  'no_participants' => 'no participants',
  '404_title' => 'page not found',
  '404_message' => 'sorry, the page you are looking for is not found. double check the URL address that you access.',
  'access_denied_notice2' => 'you do not have permission to access this content',
  'monday' => 'monday',
  'tuesday' => 'tuesday',
  'wednesday' => 'wednesday',
  'thursday' => 'thursday',
  'friday' => 'friday',
  'saturday' => 'saturday',
  'sunday' => 'sunday'
];
