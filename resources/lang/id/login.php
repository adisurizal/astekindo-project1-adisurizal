<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

return [
  'login' => 'masuk',
  'username' => 'nama pengguna',
  'insert_your_username' => 'masukan nama anda',
  'password' => 'kata sandi',
  'insert_your_password' => 'masukan kata sandi anda',
  'developed_by' => 'dikembangkan oleh',
  'username_is_required' => 'nama pengguna wajib diisi',
  'password_is_required' => 'kata sandi wajib diisi',
  'language' => 'bahasa indonesia',
  'select_language' => 'pilih bahasa',
  'incorrect_username_or_password' => 'username atau password tidak terdaftar di sistem kami',
  'your_account_is_blocked' => 'oops, akun anda terblokir. hubungi admin untuk membuka blokir',
  'you_have_logged_in_on_another_device' => 'oops, kamu telah login di perangkat lain',
  'you_are_not_allowed1' => 'anda tidak diizinkan masuk karna anda bukan peserta'
];
