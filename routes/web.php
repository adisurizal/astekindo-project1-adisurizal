<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/{locale}', 'Web\LoginController@login_get');
Route::post('/{locale}', 'Web\LoginController@login_post');

Route::get('/{locale}/login', 'Web\LoginController@login_get');
Route::post('/{locale}/login', 'Web\LoginController@login_post');

Route::get('/{locale}/secure/signout', 'Web\LoginController@signout_get');
Route::post('/{locale}/secure/signout', 'Web\LoginController@signout_post');

Route::get('/{locale}/cpanel/schedule', 'Web\CpanelController@schedule_get');
Route::post('/{locale}/cpanel/schedule', 'Web\CpanelController@schedule_post');

Route::get('/{locale}/cpanel/activities', 'Web\CpanelController@activities_get');
Route::post('/{locale}/cpanel/activities', 'Web\CpanelController@activities_post');

Route::get('/{locale}/backend/modal', 'Web\BackendController@modal_get');
Route::post('/{locale}/backend/modal', 'Web\BackendController@modal_post');
