### Cara Menjalankan Program ini [Bahasa Indonesia]
Hai, ini adalah prosedur untuk menjalankan program ini. Tutorial ini sengaja disediakan dalam Bahasa Indonesia untuk memudahkan. Silahkan ikuti langkah-langkah berikut dengan baik dan benar.

> Penting !!
> Repository ini digunakan untuk pengelolaan proyek di Astekindo.

#### Permintaan Sistem
Silahkan persiapkan alat-alat yang dibutuhkan:

1. PHP versi >= 7.2 (Jika menggunakan XAMPP, sangat disaranan untuk meginstal XAMPP versi terbaru).
2. Composer. Silahkan download disini https://getcomposer.org/download/
3. Buat skema baru / database baru di MySQL, misal namanya `astekindo`.

#### Langkah-langkah
Berikut langkah-langkah yang disarankan:

1. Lakukan clone repository dari alamat `https://gitlab.com/adisurizal/astekindo-project1-adisurizal.git`.
2. Setelah diclone, masuk ke direktori tersebut dan silahkan buka terminal / command promp (windows).
3. ketikan perintah berikut `composer install`.
4. kemudian perintah berikut `composer update`.
5. Pada direktori root, harusnya anda tidak menemukan file `.env`.
6. Buka direktori root dengan menggunakan text editor pilihan anda, misalnya ATOM atau Visual Studio Code.
7. Copy file `.env.example` dan rename menjadi `.env`.
8. Buka file `.env` tersebut dan atur parameter koneksi ke MySQL anda dengan nama database `astekindo`.
9. Buka kembali direktori root dengan menggunakan terminal anda dan ketikan perintah `php artisan key:generate`.
10. Kemudian ketikan perintah berikut `php artisan migrate`.
11. Jalankan sistem anda pada Google Chrome atau Firefox.
