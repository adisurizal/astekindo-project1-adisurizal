$(document).ready(function () {
  var sendtodelete = document.getElementById("send-to-delete");
  var sendtoedit = document.getElementById("send-to-edit");
  var sendtodetail = document.getElementById("send-to-detail");

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const get_max = urlParams.get('max');
  const get_page = urlParams.get('page');
  const get_search = urlParams.get('search');

  var link_url = document.URL;
  var parsing = link_url.split('/');
  const parsing_protocol = parsing[0];
  const parsing_mainurl = parsing[2];
  const parsing_language = parsing[3];
  const new_url_complete = parsing_protocol+'//'+parsing_mainurl+'/'+parsing_language+'/backend/modal';

  var link_uri = window.location.pathname;
  var parsing2 = link_uri.split('/');
  const parsing_cpanel = parsing2[2];
  const parsing_menu = parsing2[3];
  // jalankan fungsi jika semua dipilih
  $(".checkall").click(function() {
    if ($(this).is(":checked")) {
      var array = [];
      $(".checkone").prop("checked", true);
      $("input:checkbox[name=id_checkbox]:checked").each(function() {
        array.push($(this).val());
      });
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'users') {
        sendtodelete.value = "user,delete,"+array;
        sendtodelete.disabled = false;
        sendtoedit.value = "";
        sendtoedit.disabled = true;
        sendtodetail.value = "";
        sendtodetail.disabled = true;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'categories') {
        sendtodelete.value = "categories,delete,"+array;
        sendtodelete.disabled = false;
        sendtoedit.value = "";
        sendtoedit.disabled = true;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'products') {
        sendtodelete.value = "products,delete,"+array;
        sendtodelete.disabled = false;
        sendtoedit.value = "";
        sendtoedit.disabled = true;
        sendtodetail.value = "";
        sendtodetail.disabled = true;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'testimonies') {
        sendtodelete.value = "testimonies,delete,"+array;
        sendtodelete.disabled = false;
        sendtoedit.value = "";
        sendtoedit.disabled = true;
        sendtodetail.value = "";
        sendtodetail.disabled = true;
      }

    } else {
      $(".checkone").prop("checked", false);

      if (parsing_cpanel == 'cpanel' && parsing_menu == 'users') {
        sendtodelete.value = "";
        sendtodelete.disabled = true;
        sendtoedit.value = "";
        sendtoedit.disabled = true;
        sendtodetail.value = "";
        sendtodetail.disabled = true;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'categories') {
        sendtodelete.value = "";
        sendtodelete.disabled = true;
        sendtoedit.value = "";
        sendtoedit.disabled = true;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'products') {
        sendtodelete.value = "";
        sendtodelete.disabled = true;
        sendtoedit.value = "";
        sendtoedit.disabled = true;
        sendtodetail.value = "";
        sendtodetail.disabled = true;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'testimonies') {
        sendtodelete.value = "";
        sendtodelete.disabled = true;
        sendtoedit.value = "";
        sendtoedit.disabled = true;
        sendtodetail.value = "";
        sendtodetail.disabled = true;
      }

    }
  });
  // jalankan fungsi jika yang dipilih hanya satu dan bukan pilih semua
  $(".checkone").click(function() {
    if ($(this).is(":checked")) {
      var array = [];
      $("input:checkbox[name=id_checkbox]:checked").each(function() {
        array.push($(this).val());
      });
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'users') {
        sendtodelete.value = "user,delete,"+array;
        sendtodelete.disabled = false;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'categories') {
        sendtodelete.value = "categories,delete,"+array;
        sendtodelete.disabled = false;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'products') {
        sendtodelete.value = "products,delete,"+array;
        sendtodelete.disabled = false;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'testimonies') {
        sendtodelete.value = "testimonies,delete,"+array;
        sendtodelete.disabled = false;
      }

      var countcheckone = $('[name=id_checkbox]').length;
      var input = document.getElementsByName("id_checkbox");
      var total = 0;
      for (var i = 0; i < input.length; i++) {
        if (input[i].checked) {
          total += parseFloat(input[i].id);
        }
      }
      if (total === countcheckone) {
        $(".checkall").prop("checked", true);
      }
      if (total === 1) {
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'users') {
          sendtoedit.value = "user,edit,"+array;
          sendtoedit.disabled = false;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'categories') {
          sendtoedit.value = "categories,edit,"+array;
          sendtoedit.disabled = false;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'products') {
          sendtoedit.value = "products,edit,"+array;
          sendtoedit.disabled = false;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'testimonies') {
          sendtoedit.value = "testimonies,edit,"+array;
          sendtoedit.disabled = false;
        }

        if (parsing_cpanel == 'cpanel' && parsing_menu == 'users') {
          sendtodetail.value = "user,detail,"+array;
          sendtodetail.disabled = false;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'products') {
          sendtodetail.value = "products,detail,"+array;
          sendtodetail.disabled = false;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'testimonies') {
          sendtodetail.value = "testimonies,detail,"+array;
          sendtodetail.disabled = false;
        }

      }
      if (total > 1) {
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'users') {
          sendtoedit.value = "";
          sendtoedit.disabled = true;
          sendtodetail.value = "";
          sendtodetail.disabled = true;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'categories') {
          sendtoedit.value = "";
          sendtoedit.disabled = true;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'products') {
          sendtoedit.value = "";
          sendtoedit.disabled = true;
          sendtodetail.value = "";
          sendtodetail.disabled = true;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'testimonies') {
          sendtoedit.value = "";
          sendtoedit.disabled = true;
          sendtodetail.value = "";
          sendtodetail.disabled = true;
        }

      }
    } else {
      $(".checkall").prop("checked", false);
      var array = [];
      $("input:checkbox[name=id_checkbox]:checked").each(function() {
        array.push($(this).val());
      });
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'users') {
        sendtodelete.value = "user,delete,"+array;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'categories') {
        sendtodelete.value = "categories,delete,"+array;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'products') {
        sendtodelete.value = "products,delete,"+array;
      }
      if (parsing_cpanel == 'cpanel' && parsing_menu == 'testimonies') {
        sendtodelete.value = "testimonies,delete,"+array;
      }

      var input = document.getElementsByName("id_checkbox");
      var total = 0;
      for (var i = 0; i < input.length; i++) {
        if (input[i].checked) {
          total += parseFloat(input[i].id);
        }
      }
      if (total === 1) {
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'users') {
          sendtoedit.value = "user,edit,"+array;
          sendtoedit.disabled = false;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'categories') {
          sendtoedit.value = "categories,edit,"+array;
          sendtoedit.disabled = false;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'products') {
          sendtoedit.value = "products,edit,"+array;
          sendtoedit.disabled = false;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'testimonies') {
          sendtoedit.value = "testimonies,edit,"+array;
          sendtoedit.disabled = false;
        }

        if (parsing_cpanel == 'cpanel' && parsing_menu == 'users') {
          sendtodetail.value = "user,detail,"+array;
          sendtodetail.disabled = false;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'products') {
          sendtodetail.value = "products,detail,"+array;
          sendtodetail.disabled = false;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'testimonies') {
          sendtodetail.value = "testimonies,detail,"+array;
          sendtodetail.disabled = false;
        }

      }
      if (total > 1) {
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'users') {
          sendtoedit.value = "";
          sendtoedit.disabled = true;
          sendtodetail.value = "";
          sendtodetail.disabled = true;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'categories') {
          sendtoedit.value = "";
          sendtoedit.disabled = true;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'products') {
          sendtoedit.value = "";
          sendtoedit.disabled = true;
          sendtodetail.value = "";
          sendtodetail.disabled = true;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'testimonies') {
          sendtoedit.value = "";
          sendtoedit.disabled = true;
          sendtodetail.value = "";
          sendtodetail.disabled = true;
        }

      }
      if (total === 0) {
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'users') {
          sendtodelete.value = "";
          sendtodelete.disabled = true;
          sendtoedit.value = "";
          sendtoedit.disabled = true;
          sendtodetail.value = "";
          sendtodetail.disabled = true;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'categories') {
          sendtodelete.value = "";
          sendtodelete.disabled = true;
          sendtoedit.value = "";
          sendtoedit.disabled = true;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'products') {
          sendtodelete.value = "";
          sendtodelete.disabled = true;
          sendtoedit.value = "";
          sendtoedit.disabled = true;
          sendtodetail.value = "";
          sendtodetail.disabled = true;
        }
        if (parsing_cpanel == 'cpanel' && parsing_menu == 'testimonies') {
          sendtodelete.value = "";
          sendtodelete.disabled = true;
          sendtoedit.value = "";
          sendtoedit.disabled = true;
          sendtodetail.value = "";
          sendtodetail.disabled = true;
        }

      }
    }
  });
})
