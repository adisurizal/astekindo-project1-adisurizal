<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LanguageController extends Controller {

  public function select_language($locale) {
    return ($locale == 'en' or $locale == 'id') ? '1' : '0' ;
  }

  public function default_language() {
    $url = request()->fullUrl();
    $explode = explode('/', $url);
    $array = '';
    for ($i=0; $i < count($explode); $i++) {
      if ($i == 3) {
        $array .= 'en/';
      } else {
        $array .= $explode[$i].'/';
      }
    }
    return redirect($array);
  }

}
