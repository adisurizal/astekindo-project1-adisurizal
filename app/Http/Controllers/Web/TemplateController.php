<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App;

class TemplateController extends Controller {

  /*
  // this is template for language
  public function login_get($locale) {
    if ($this->LanguageController->select_language($locale) == '1') {
      App::setLocale($locale);
      // write your code here ...
    } else {
      return $this->LanguageController->default_language();
    }
  }

  Berikut code untuk membuat Hash di Laravel
  use Illuminate\Support\Facades\Hash;
  $password_hash = Hash::make($password);

  catatan kode:
  $eloquent = Users::select('users.*', 'uji_peserta.nama as nama_peserta_x', 'uji_peserta.no_hp as nohp_peserta_x', 'uji_ms_customer.*')
                   ->join('uji_peserta', 'uji_peserta.user_id', '=', 'users.id')
                   ->join('uji_ms_customer', 'uji_ms_customer.id', '=', 'uji_peserta.customer')
                   ->where('users.id', Session::get('session_id_signin'));

  $eloquent2 = Users::select('users.*', 'uji_peserta.*', 'uji_jadwal.*', 'badan_usaha.*', 'ms_bidang.*', 'ms_bid_sertifikat_alat.*', 'uji_jadwal.id as id_jadwal',
                             'uji_jadwal.tgl_awal as tgl_awal_kegiatan', 'uji_jadwal.tgl_akhir as tgl_akhir_kegiatan', 'ms_bid_sertifikat_alat.id as id_sertifikat_alat',
                             'uji_jadwal.id_kota as id_kota_ini', 'uji_jadwal.id_koordinator as id_koordinator_x')
                    ->join('uji_peserta', 'uji_peserta.user_id', '=', 'users.id')
                    ->join('uji_jadwal', 'uji_jadwal.id', '=', 'uji_peserta.id_jadwal')
                    ->join('badan_usaha', 'badan_usaha.id', '=', 'uji_jadwal.id_pjk3')
                    ->join('ms_bidang', 'ms_bidang.id', '=', 'uji_jadwal.id_bidang')
                    ->join('ms_bid_sertifikat_alat', 'ms_bid_sertifikat_alat.id', '=', 'uji_jadwal.id_sert_alat')
                    ->where('users.id', Session::get('session_id_signin'));
  */

}
