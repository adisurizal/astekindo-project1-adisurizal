<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ConfigurationController extends Controller {

  public function generate_random_string($length) {
    $characters = '0123456789';
    $characters_length = strlen($characters);
    $random_string = '';
    for ($i = 0; $i < $length; $i++) {
        $random_string .= $characters[rand(0, $characters_length - 1)];
    }
    return $random_string;
  }

  public function divider($value) {
    if ($value == 'a') return 10;
    elseif ($value == 'b') return 50;
    elseif ($value == 'c') return 100;
    else return 10;
  }

  public function maxpage() {
    $value = 4;
    $median = $value/2;
    $ceil_median = ceil($median);
    $value_before = $ceil_median-1;
    $value_after = $value-$ceil_median;
    return [$value, $value_before, $value_after];
  }

  public function detect_mobile() {
    return strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') ? true : false;
  }

  public function optional() {
    return [
      '',                                                  // you can use '/public' or ''
      'PJK3',                                              // you can change this name
      '',                                                  // you can change this name
      'P3SM',                                              // please do not change this name
      'https://srtf.p3sm.or.id/',                          // please do not change this this URL
      '',                                                  // this is Telegram token
      '1.0',                                               // version
      'Pusat Pembinaan Pelatihan & Sertifikasi Mandiri'    // slogan 2
    ];
  }

}
