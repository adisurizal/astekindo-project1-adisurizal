<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// use other controller
use App\Http\Controllers\Web\LanguageController;
use App\Http\Controllers\Web\ConfigurationController;

// use models
use App\Models\Badanusaha;
use App\Models\Instruktur;
use App\Models\Msbidang;
use App\Models\Msbidsertifikatalat;
use App\Models\Msjenisnarasumber;
use App\Models\Mskota;
use App\Models\Msmodul;
use App\Models\Msprovinsi;
use App\Models\Mstuk;
use App\Models\Personal;
use App\Models\Ujijadwal;
use App\Models\Ujijadwalpny;
use App\Models\Ujijadwalrundown;
use App\Models\Ujimscustomer;
use App\Models\Ujimspeserta;
use App\Models\Ujipeserta;
use App\Models\Users;

use Session;
use App;

class CpanelController extends Controller {

  protected $LanguageController, $ConfigurationController;

  public function __construct(LanguageController $LanguageController, ConfigurationController $ConfigurationController) {
    $this->LanguageController = $LanguageController;
    $this->ConfigurationController = $ConfigurationController;
  }

  public function activities_get($locale) {
    if ($this->LanguageController->select_language($locale) == '1') {
      App::setLocale($locale);
      if (Session::get('session_id_signin') == null) return redirect(url('/'.$locale.'/secure/signout'));
      if (isset($_GET['id'])) {
        $check_id = App\Models\Ujipeserta::where('id', $_GET['id']);
        if ($check_id->get()->count() == '0') {
          // return to view
          return view('web.cpanel.access-denied', [
            'user_online' => $this->user_online(),
            'optional' => $this->ConfigurationController->optional(),
            'detect_mobile' => $this->ConfigurationController->detect_mobile(),
            'locale' => $locale,
            'image_path' => url($this->ConfigurationController->optional()[0].'/vendor/astekindo').'/img/404.jpg',
            'message_error' => ucfirst(trans('cpanel.404_message')),
            'title' => ucfirst(trans('cpanel.404_title'))
          ]);
        } else {
          $check_id_first = $check_id->first();
          if ($check_id_first['user_id'] != Session::get('session_id_signin')) {
            // return to view
            return view('web.cpanel.access-denied', [
              'user_online' => $this->user_online(),
              'optional' => $this->ConfigurationController->optional(),
              'detect_mobile' => $this->ConfigurationController->detect_mobile(),
              'locale' => $locale,
              'image_path' => url($this->ConfigurationController->optional()[0].'/vendor/astekindo').'/img/403.jpg',
              'message_error' => ucfirst(trans('cpanel.access_denied_notice2')),
              'title' => ucfirst(trans('cpanel.access_denied'))
            ]);
          } else {
            // return to view
            return view('web.cpanel.activities', [
              'user_online' => $this->user_online(),
              'optional' => $this->ConfigurationController->optional(),
              'detect_mobile' => $this->ConfigurationController->detect_mobile(),
              'locale' => $locale
            ]);
          }
        }
      } else {
        // return to view
        return view('web.cpanel.access-denied', [
          'user_online' => $this->user_online(),
          'optional' => $this->ConfigurationController->optional(),
          'detect_mobile' => $this->ConfigurationController->detect_mobile(),
          'locale' => $locale,
          'image_path' => url($this->ConfigurationController->optional()[0].'/vendor/astekindo').'/img/403.jpg',
          'message_error' => ucfirst(trans('cpanel.access_denied_notice1')),
          'title' => ucfirst(trans('cpanel.access_denied'))
        ]);
      }
    } else {
      return $this->LanguageController->default_language();
    }
  }

  public function activities_post() {
    #
  }

  public function schedule_get($locale) {
    if ($this->LanguageController->select_language($locale) == '1') {
      App::setLocale($locale);
      if (Session::get('session_id_signin') == null) return redirect(url('/'.$locale.'/secure/signout'));
      $eloquent = Users::where('id', Session::get('session_id_signin'));
      // return to view
      return view('web.cpanel.schedule', [
        'user_online' => $this->user_online(),
        'optional' => $this->ConfigurationController->optional(),
        'detect_mobile' => $this->ConfigurationController->detect_mobile(),
        'locale' => $locale,
        'eloquent' => $eloquent
      ]);
    } else {
      return $this->LanguageController->default_language();
    }
  }

  public function schedule_post(Request $request, $locale) {
    #
  }

  private function user_online() {
    // if the session_id_signin is set
    if (null !== Session::get('session_id_signin')) {
      $id = Session::get('session_id_signin');
    } else {
      // you can enter the user ids value you want
      $id = '';
    }
    $eloquent = Users::where('is_active', '1')->where('id', $id);
    return $eloquent->first();
  }

}
