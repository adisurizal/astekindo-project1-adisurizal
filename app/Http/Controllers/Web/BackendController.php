<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;
use App;

class BackendController extends Controller {

  public function modal_get() {
    return 'access denied';
  }

  public function modal_post(Request $request, $locale) {
    App::setLocale($locale);
    $id = htmlentities(addslashes($request->input('id')));
    if (isset($id)) {
      if ($id == 'language') {
        return view('web.modal.language', [
          'id' => $id,
          'locale' => $locale
        ]);
      } elseif ($id == 'signout') {
        return view('web.modal.signout', [
          'id' => $id,
          'locale' => $locale
        ]);
      } elseif ($id == 'myprofile') {
        return view('web.modal.myprofile', [
          'id' => $id,
          'locale' => $locale
        ]);
      }
    } else {
      return 'access denied';
    }
  }

}
