<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

// use other controller
use App\Http\Controllers\Web\LanguageController;
use App\Http\Controllers\Web\ConfigurationController;

// use models
use App\Models\Users;

use Session;
use App;

class LoginController extends Controller {

  protected $LanguageController, $ConfigurationController;

  public function __construct(LanguageController $LanguageController, ConfigurationController $ConfigurationController) {
    $this->LanguageController = $LanguageController;
    $this->ConfigurationController = $ConfigurationController;
  }

  public function login_get($locale) {
    App::setLocale($locale);
    if ($this->LanguageController->select_language($locale) == '1') {
      // this is code for session login
      if (null !== (Session::get('session_id_signin'))) return redirect(url('/'.$locale.'/cpanel/schedule'));
      // return to view
      return view('web.public.login', [
        'optional' => $this->ConfigurationController->optional(),
        'locale' => $locale
      ]);
    } else {
      return $this->LanguageController->default_language();
    }
  }

  public function login_post(Request $request, $locale) {
    App::setLocale($locale);
    if (isset($request->username) && isset($request->password)) {
      $username = htmlentities(addslashes($request->username));
      $password = htmlentities(addslashes($request->password));
      $auth_login = [
        'username' => $username,
        'password' => $password
      ];
      if (auth()->attempt($auth_login)) {
        $eloquent = Users::where('username', $username);
        $is_active = $eloquent->first()['is_active'];
        $is_login = $eloquent->first()['is_login'];
        $role_id = $eloquent->first()['role_id'];
        $id = $eloquent->first()['id'];
        if ($is_active != '1') {
          session([
            'session_class_login' => 'alert-danger',
            'session_icon_login' => 'fa-exclamation-triangle',
            'session_message_login' => ucfirst(trans('login.your_account_is_blocked')),
            'session_data_login_username' => $username,
            'session_data_login_password' => $password
          ]);
          return redirect(url('/'.$locale.'/login'));
        } elseif ($is_login != NULL) {
          session([
            'session_class_login' => 'alert-warning',
            'session_icon_login' => 'fa-exclamation-triangle',
            'session_message_login' => ucfirst(trans('login.you_have_logged_in_on_another_device')),
            'session_data_login_username' => $username,
            'session_data_login_password' => $password
          ]);
          return redirect(url('/'.$locale.'/login'));
        } elseif ($role_id != '13') {
          session([
            'session_class_login' => 'alert-danger',
            'session_icon_login' => 'fa-exclamation-triangle',
            'session_message_login' => ucfirst(trans('login.you_are_not_allowed1')),
            'session_data_login_username' => $username,
            'session_data_login_password' => $password
          ]);
          return redirect(url('/'.$locale.'/login'));
        } else {
          // update is_login
          Users::where('id', $id)->update([
              'is_login' => '1',
              'last_login' => now()
          ]);
          session([
            'session_id_signin' => $eloquent->first()['id'],
          ]);
          return redirect(url('/'.$locale.'/cpanel/schedule'));
        }
      } else {
        session([
          'session_class_login' => 'alert-danger',
          'session_icon_login' => 'fa-exclamation-triangle',
          'session_message_login' => ucfirst(trans('login.incorrect_username_or_password')),
          'session_data_login_username' => $username,
          'session_data_login_password' => $password
        ]);
        return redirect(url('/'.$locale.'/login'));
      }
    } else {
      return 'access denied';
    }
  }

  public function signout_get($locale) {
    if (null !== (Session::get('session_id_signin'))) {
      // update is_login
      Users::where('id', Session::get('session_id_signin'))->update([
          'is_login' => NULL
      ]);
    }
    Session::flush();
    return redirect(url('/'.$locale.'/login'));
  }

  public function signout_post(Request $request, $locale) {
    return 'access denied';
  }

}
