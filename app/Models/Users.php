<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model {

  protected $table = 'users';

  protected $fillable = [
      'id',
      'username',
      'prov_naker',
      'jenis_usaha',
      'email',
      'hint',
      'password',
      'name',
      'last_login',
      'role_id',
      'is_active',
      'is_login',
      'remember_token',
      'created_by',
      'created_at',
      'updated_by',
      'updated_at',
      'deleted_by',
      'deleted_at'
  ];

}
