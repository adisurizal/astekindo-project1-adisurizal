<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Msbidang extends Model {

  protected $table = 'ms_bidang';

  protected $fillable = [
      'id',
      'id_jns_usaha',
      'kode_bidang',
      'nama_bidang',
      'singkat_bidang',
      'keterangan',
      'is_actived',
      'created_by',
      'created_at',
      'updated_by',
      'updated_at',
      'deleted_by',
      'deleted_at'
  ];

}
