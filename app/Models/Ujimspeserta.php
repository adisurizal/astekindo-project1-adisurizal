<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ujimspeserta extends Model {

  protected $table = 'uji_ms_peserta';

  protected $fillable = [
      'id',
      'nik',
      'nama',
      'jenis_kelamin',
      'tmp_lahir',
      'tgl_lahir',
      'alamat_ktp',
      'prov',
      'kota',
      'no_hp',
      'email',
      'id_customer',
      'jabatan',
      'f_ktp',
      'f_ijasah',
      'foto',
      'user_id',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at'
  ];

}
