<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mskota extends Model {

  protected $table = 'ms_kota';

  protected $fillable = [
      'id',
      'provinsi_id',
      'nama',
      'ibu_kota',
      'singkatan_kota',
      'is_active',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at'
  ];

}
