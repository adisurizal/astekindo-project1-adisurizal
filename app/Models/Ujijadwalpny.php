<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ujijadwalpny extends Model {

  protected $table = 'uji_jadwal_pny';

  protected $fillable = [
      'id',
      'id_jadwal',
      'id_bu',
      'nama_bu',
      'singkat_bu',
      'id_prov',
      'id_kota',
      'nama_pimp',
      'no_pimp',
      'kontak_p',
      'no_kontak_p',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at'
  ];

}
