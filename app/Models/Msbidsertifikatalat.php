<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Msbidsertifikatalat extends Model {

  protected $table = 'ms_bid_sertifikat_alat';

  protected $fillable = [
      'id',
      'id_bid',
      'id_sub_bidang',
      'kode_srtf_alat',
      'nama_srtf_alat',
      'keterangan',
      'is_actived',
      'created_by',
      'created_at',
      'updated_by',
      'updated_at',
      'deleted_by',
      'deleted_at'
  ];

}
