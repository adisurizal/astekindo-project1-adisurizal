<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Msmodul extends Model {

  protected $table = 'ms_modul';

  protected $fillable = [
      'id',
      'id_bid_srtf_alat',
      'modul',
      'jp',
      'hari',
      'persyaratan',
      'materi_modul',
      'materi_ajar',
      'link',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at'
  ];

}
