<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Msprovinsi extends Model {

  protected $table = 'ms_provinsi';

  protected $fillable = [
      'id',
      'nama',
      'nama_singkat',
      'ibu_kota_provinsi',
      'is_active',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at'
  ];

}
