<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mstuk extends Model {

  protected $table = 'ms_tuk';

  protected $fillable = [
      'id',
      'pjk3',
      'nama_tuk',
      'prov',
      'kota',
      'alamat',
      'email',
      'web',
      'no_hp',
      'pengelola',
      'kontak_p',
      'jab_kontak_p',
      'no_hp_kontak_p',
      'email_kontak_p',
      'npwp',
      'no_rek',
      'id_bank',
      'nama_rek',
      'keterangan',
      'is_ob',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at'
  ];

}
