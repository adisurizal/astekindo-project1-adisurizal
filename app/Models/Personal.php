<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model {

  protected $table = 'personal';

  protected $fillable = [
      'id',
      'user_id',
      'nik',
      'nama',
      'jns_kelamin',
      'alamat',
      'hp_wa',
      'email_p',
      'kode_kota',
      'temp_lahir',
      'tgl_lahir',
      'agama',
      'id_ptkp',
      'status_pernikahan',
      'bpjs_no',
      'bpjs_pdf',
      'no_rek',
      'nama_rek',
      'id_bank',
      'npwp',
      'npwp_pdf',
      'ktp_pdf',
      'foto_pdf',
      'reff_p',
      'status_p',
      'keterangan',
      'is_actived',
      'created_by',
      'created_at',
      'updated_by',
      'updated_at',
      'deleted_by',
      'deleted_at'
  ];

}
