<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ujijadwal extends Model {

  protected $table = 'uji_jadwal';

  protected $fillable = [
      'id',
      'id_ms_jadwal_header',
      'id_pjk3',
      'no_kgt',
      'tgl_pengajuan',
      'id_jenis_usaha',
      'id_bidang',
      'id_sert_alat',
      'no_ijin_bid',
      'tgl_akhir_ijin_bid',
      'jns_mktg',
      'id_tim_mktg',
      'jns_kgt',
      'id_tim_prod',
      'id_prov',
      'id_kota',
      'id_tuk_tl',
      'id_tuk_o',
      'tgl_awal',
      'tgl_akhir',
      'id_koordinator',
      'id_pj',
      'id_pengawas',
      'total_fee',
      'id_klp_peserta',
      'id_klp_soal_pg',
      'id_klp_soal_essay',
      'is_kelompok',
      'durasi_ujian',
      'mulai_ujian',
      'akhir_ujian',
      'pdf_tugas',
      'pdf_jadwal',
      'f_soal_pg',
      'f_soal_essay',
      'f_peserta',
      'tgl_upload_tugas',
      'batas_up_makalah',
      'f_pkl',
      'l_pkl',
      'batas_up_tugas',
      'remarks',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at'
  ];

}
