<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ujimscustomer extends Model {

  protected $table = 'uji_ms_customer';

  protected $fillable = [
      'id',
      'id_jenis_usaha',
      'id_tim_m',
      'nama_bu',
      'nama_singkat_bu',
      'id_bu',
      'level',
      'level_atas',
      'alamat',
      'prop',
      'kota',
      'no_telp',
      'email',
      'instansi_reff',
      'web',
      'nama_pimp',
      'jab_pimp',
      'hp_pimp',
      'email_pimp',
      'nama_kp',
      'jab_kp',
      'hp_kp',
      'email_kp',
      'npwp',
      'npwp_f',
      'no_rek',
      'nama_rek',
      'id_bank',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at'
  ];

}
