<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Badanusaha extends Model {

  protected $table = 'badan_usaha';

  protected $fillable = [
      'id',
      'level_atas',
      'status_kantor',
      'prop_naker',
      'kode_bu',
      'jns_usaha',
      'id_bentuk_usaha',
      'nama_bu',
      'singkat_bu',
      'alamat',
      'id_prop',
      'id_kota',
      'telp',
      'email',
      'web',
      'instansi_reff',
      'nama_pimp',
      'jab_pimp',
      'hp_pimp',
      'email_pimp',
      'kontak_p',
      'no_kontak_p',
      'jab_kontak_p',
      'email_kontak_p',
      'no_rek',
      'nama_rek',
      'id_bank',
      'npwp',
      'npwp_pdf',
      'keterangan',
      'is_actived',
      'created_by',
      'created_at',
      'updated_by',
      'updated_at',
      'deleted_by',
      'deleted_at'
  ];

}
