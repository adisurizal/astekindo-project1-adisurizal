<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ujipeserta extends Model {

  #
  protected $table = 'uji_peserta';

  protected $fillable = [
      'id',
      'id_jadwal',
      'id_tim_m',
      'id_gol_hrg_m',
      'customer',
      'nama_kontak_p',
      'no_kontak_p',
      'nik',
      'nama',
      'jenis_kelamin',
      'tmp_lahir',
      'tgl_lahir',
      'alamat_ktp',
      'prov',
      'kota',
      'no_hp',
      'email',
      'jabatan',
      'f_ktp',
      'f_ijasah',
      'foto',
      'f_suker',
      'durasi',
      'mulai_ujian',
      'user_id',
      'status_sms',
      'text_sms',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at'
  ];

}
