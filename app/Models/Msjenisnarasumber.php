<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Msjenisnarasumber extends Model {

  protected $table = 'ms_jenis_narasumber';

  protected $fillable = [
      'id',
      'nama',
      'singkatan',
      'keterangan',
      'type',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at'
  ];

}
