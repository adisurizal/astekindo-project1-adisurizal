<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ujijadwalrundown extends Model {

  protected $table = 'uji_jadwal_rundown';

  protected $fillable = [
      'id',
      'id_jadwal',
      'hari',
      'tanggal',
      'start',
      'end',
      'id_modul',
      'id_sesi',
      'tipe_sesi',
      'id_jenis_narsum',
      'id_instruktur',
      'nama_narsum',
      'hp_narsum',
      'prov_narsum',
      'status',
      'fee',
      'is_ttd_ba',
      'is_hasil_akhir',
      'f_materi_modul',
      'f_materi_modul_start',
      'f_materi_modul_end',
      'f_materi_modul_durasi',
      'f_materi_ajar',
      'f_materi_ajar_start',
      'f_materi_ajar_end',
      'f_materi_ajar_durasi',
      'f_quis_awal_pg',
      'f_quis_awal_es',
      'f_quis_awal_start',
      'f_quis_awal_end',
      'f_quis_awal_durasi',
      'f_quis_akhir_pg',
      'f_quis_akhir_es',
      'f_quis_akhir_start',
      'f_quis_akhir_end',
      'f_quis_akhir_durasi',
      'f_video_pkl',
      'f_video_pkl_start',
      'f_video_pkl_end',
      'f_video_pkl_durasi',
      'f_materi_tambahan',
      'f_materi_tambahan_start',
      'f_materi_tambahan_end',
      'f_materi_tambahan_durasi',
      'f_tugas_narsum',
      'f_tugas_narsum_start',
      'f_tugas_narsum_end',
      'f_tugas_narsum_durasi',
      'f_ujian_akhir_pg',
      'f_ujian_akhir_es',
      'f_ujian_akhir_start',
      'f_ujian_akhir_end',
      'f_ujian_akhir_durasi',
      'f_ujian_her_pg',
      'f_ujian_her_es',
      'f_ujian_her_start',
      'f_ujian_her_end',
      'f_ujian_her_durasi',
      'jumlah_tm',
      'remarks',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at'
  ];

}
