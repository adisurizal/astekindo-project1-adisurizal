<?php

/*
author  : adisurizal
email   : adisurizal.cyber@outlook.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instruktur extends Model {

  protected $table = 'instruktur';

  protected $fillable = [
      'id',
      'id_pjk3',
      'prov_naker',
      'id_personal',
      'id_bidang',
      'id_bid_sertifikat',
      'id_jenis_narsum',
      'fee',
      'pdf_srtf',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at',
      'keterangan'
  ];

}
